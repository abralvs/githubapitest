﻿using Model;
using Service.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace Service
{

    public class UsuarioService : IUsuarioService
    {
        private readonly string _urlBase;
        public UsuarioService(string url)
        {
            _urlBase = url;
        }

        public async Task<UsuarioModel> GetUserByName(string usuario)
        {
            var usuarioModel = new UsuarioModel();

            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri(_urlBase);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/vnd.github.v3+json"));
                client.DefaultRequestHeaders.Add("User-Agent", "Avonale Test Github API");
                string url = "/users/" + usuario;

                var reposString = await client.GetStringAsync(url);
                usuarioModel = JsonSerializer.Deserialize<UsuarioModel>(reposString);

            }
            return usuarioModel;
        }
    }
}
