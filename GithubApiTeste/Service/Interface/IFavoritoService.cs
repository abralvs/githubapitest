﻿using Model;
using System.Collections.Generic;

namespace Service.Interface
{
    public interface IFavoritoService
    {
        List<FavoritoModel> GetAll();
        FavoritoModel GetById(int id);
        bool Remove(string nomeCompleto);
        bool Insert(FavoritoModel favoritoModel);
    }
}
