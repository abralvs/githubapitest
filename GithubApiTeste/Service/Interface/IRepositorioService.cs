﻿using Model;
using Model.AuxModel;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Service.Interface
{
    public interface IRepositorioService
    {
        Task<RepositorioModel> GetRepositorioByFullName(string nomeCompleto);
        Task<List<RepositorioModel>> GetAllByUser(string usuario);
        Task<SearchResultAuxModel> GetSearchRepo(string searchValue);
        Task<List<UsuarioModel>> GetContribuitors(string nomeCompleto);
    }
}