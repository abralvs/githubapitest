﻿using Model;
using Model.AuxModel;
using Service.Interface;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text.Json;
using System.Threading.Tasks;

namespace Service
{
    public class RepositorioService : IRepositorioService
    {
        private readonly string _urlBase;
        public RepositorioService(string url)
        {
            _urlBase = url;
        }

        /// <summary>
        /// Este método retorna os dados dos repositórios pelo nome/login do usuário
        /// </summary>
        /// <param name="usuario">parametro nome/login do usuário</param>
        /// <returns>A lista de repositórios que pertencem ao usuário fornecido</returns>
        public async Task<List<RepositorioModel>> GetAllByUser(string usuario)
        {
            var repositoriosUsuario = new List<RepositorioModel>();

            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri(_urlBase);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/vnd.github.v3+json"));
                client.DefaultRequestHeaders.Add("User-Agent", "Avonale Test Github API");
                string url = "/users/" + usuario + "/repos";

                var reposString = await client.GetStringAsync(url);
                repositoriosUsuario = JsonSerializer.Deserialize<List<RepositorioModel>>(reposString);

            }
            return repositoriosUsuario;
        }

        public async Task<List<UsuarioModel>> GetContribuitors(string nomeCompleto)
        {
            var contribuidores = new List<UsuarioModel>();

            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri(_urlBase);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/vnd.github.v3+json"));
                client.DefaultRequestHeaders.Add("User-Agent", "Avonale Test Github API");
                string url = "/repos/" + nomeCompleto + "/contributors";


                try
                {
                    var reposString = await client.GetStringAsync(url);
                    contribuidores = JsonSerializer.Deserialize<List<UsuarioModel>>(reposString);
                }
                catch(Exception e)
                {
                    return null;
                }

            }
            return contribuidores;
        }

        /// <summary>
        /// Esse método retorna os dados dos repostorios pelo nome Completo do repositorio
        /// </summary>
        /// <param name="nomeCompleto">nome Completo do Repositório</param>
        /// <returns></returns>
        public async Task<RepositorioModel> GetRepositorioByFullName(string nomeCompleto)
        {
            var repositoriosUsuario = new RepositorioModel();

            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri(_urlBase);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/vnd.github.v3+json"));
                client.DefaultRequestHeaders.Add("User-Agent", "Avonale Test Github API");
                string url = _urlBase + "/repos/" + nomeCompleto;

                try
                {
                    var reposString = await client.GetStringAsync(url);
                    repositoriosUsuario = JsonSerializer.Deserialize<RepositorioModel>(reposString);
                }
                catch(Exception e)
                {
                    return null;
                }

            }
            return repositoriosUsuario;
        }

        public async Task<SearchResultAuxModel> GetSearchRepo(string searchValue)
        {
            var repositorios = new SearchResultAuxModel();

            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri(_urlBase);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/vnd.github.v3+json"));
                client.DefaultRequestHeaders.Add("User-Agent", "Avonale Test Github API");
                string url = "/search/repositories?q=" + searchValue;

                var reposString = await client.GetStringAsync(url);

                repositorios = JsonSerializer.Deserialize<SearchResultAuxModel>(reposString);

            }
            return repositorios;
        }
    }
}