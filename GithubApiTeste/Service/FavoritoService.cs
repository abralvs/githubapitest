﻿using Model;
using Persistence;
using Service.Interface;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Service
{
    public class FavoritoService : IFavoritoService
    {
        private readonly SQLiteDBContext _context;
        public FavoritoService(SQLiteDBContext context)
        {
            _context = context;
        }


        public List<FavoritoModel> GetAll()
        {
            var favoritos = _context.Favoritos.Select(f => new FavoritoModel
            {
                Id = f.Id,
                Nome = f.Nome,
                NomeCompleto = f.NomeCompleto,
                Proprietario = f.Proprietario
            }).ToList();
            return favoritos;
        }

        public FavoritoModel GetById(int id)
        {
            throw new NotImplementedException();
        }

        public bool Insert(FavoritoModel favoritoModel)
        {
            _context.Favoritos.Add(new Favorito
            {
                Id = favoritoModel.Id,
                Nome = favoritoModel.Nome,
                NomeCompleto = favoritoModel.NomeCompleto,
                Proprietario = favoritoModel.Proprietario
            });
            return _context.SaveChanges() == 1;
        }

        public bool Remove(string nomeCompleto)
        {

            var favorito = _context.Favoritos.SingleOrDefault(o => o.NomeCompleto.Equals(nomeCompleto));

            if (favorito != null)
                _context.Favoritos.Remove(favorito);
            return _context.SaveChanges() == 1;
        }
    }
}
