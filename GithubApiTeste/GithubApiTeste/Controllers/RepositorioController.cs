﻿using Model;
using Service.Interface;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace GithubApiTeste.Controllers
{
    public class RepositorioController : Controller
    {
        private readonly IRepositorioService _repositorioService;
        private readonly IFavoritoService _favService;
        public RepositorioController(IRepositorioService repositorioService, IFavoritoService favService)
        {
            _repositorioService = repositorioService;
            _favService = favService;
        }

        public ActionResult Index()
        {
            return View();
        }

        public async Task<ActionResult> Details(string nomeCompleto)
        {
            if (string.IsNullOrEmpty(nomeCompleto))
                return RedirectToAction("Index", "Home");

            var repo = await _repositorioService.GetRepositorioByFullName(nomeCompleto);
            repo.Contribuidores = await _repositorioService.GetContribuitors(nomeCompleto);

            return View(repo);
        }

        [HttpGet]
        public async Task<ActionResult> ReposUsuario(string usuario)
        {
            if (string.IsNullOrEmpty(usuario))
                return RedirectToAction("Index", "Home");

            ViewBag.IsMe = usuario.Equals("abralvs");
            ViewBag.Favoritos = _favService.GetAll();
            var repos = await _repositorioService.GetAllByUser(usuario);
            return View(repos);
        }

        [HttpPost]
        public async Task<ActionResult> ReposUsuario()
        {
            var searchValue = HttpContext.Request.Form["searchValue"];
            if (string.IsNullOrEmpty(searchValue))
                return RedirectToAction("Index", "Home");

            ViewBag.Favoritos = _favService.GetAll();
            var searchResult = await _repositorioService.GetSearchRepo(searchValue);
            ViewBag.SearchTotal = searchResult.TotalRepo;
            return View(searchResult.Repositorios);
        }

        public async Task<ActionResult> Favoritar(string nomeCompleto)
        {
            var repo = await _repositorioService.GetRepositorioByFullName(nomeCompleto);
            var favoritos = _favService.GetAll();
            if (favoritos.Any(f => f.NomeCompleto.Equals(repo.NomeCompleto)))
            {

                var removeu = _favService.Remove(repo.NomeCompleto);
                if (removeu)
                    TempData["mensagemSucesso"] = "Removeu o favorito com sucesso.";
                else
                    TempData["mensagemErro"] = "Houve um problema ao remover favorito.";
            }
            else
            {
                var inseriu = _favService.Insert(new FavoritoModel { Id = repo.Id, Nome = repo.Nome, NomeCompleto = repo.NomeCompleto, Proprietario = repo.Proprietario.Login });
                if (inseriu)
                    TempData["mensagemSucesso"] = "Inseriu o favorito com sucesso.";
                else
                    TempData["mensagemErro"] = "Houve um problema ao adicionar favorito.";
            }
            return RedirectToAction("Favoritos", "Repositorio");
        }

        public ActionResult Favoritos()
        {
            var favoritos = _favService.GetAll();
            return View(favoritos);
        }
    }
}