﻿using Service.Interface;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace GithubApiTeste.Controllers
{
    public class HomeController : Controller
    {
        private readonly IUsuarioService _usuarioService;
        public HomeController(IUsuarioService usuarioService)
        {
            _usuarioService = usuarioService;
        }
        public async Task<ActionResult> Index()
        {
            var usuario = await _usuarioService.GetUserByName("abralvs");
            return View(usuario);
        }
    }
}