using Persistence;
using Service;
using Service.Interface;
using System.Web.Mvc;
using Unity;
using Unity.Injection;
using Unity.Mvc5;

namespace GithubApiTeste
{
    public static class UnityConfig
    {
        public static void RegisterComponents()
        {
            var container = new UnityContainer();
            container.RegisterType<IUsuarioService, UsuarioService>(new InjectionConstructor("https://api.github.com"));
            container.RegisterType<IRepositorioService, RepositorioService>(new InjectionConstructor("https://api.github.com"));
            container.RegisterType<IFavoritoService, FavoritoService>(new InjectionConstructor(new SQLiteDBContext()));


            DependencyResolver.SetResolver(new UnityDependencyResolver(container));
        }
    }
}