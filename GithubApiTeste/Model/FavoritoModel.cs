﻿namespace Model
{
    public class FavoritoModel
    {
        public int Id { set; get; }
        public string Nome { set; get; }
        public string NomeCompleto { set; get; }
        public string Proprietario { set; get; }
    }
}
