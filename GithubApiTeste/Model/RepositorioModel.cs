﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace Model
{
    public class RepositorioModel
    {
        [JsonPropertyName("id")]
        public int Id { set; get; }
        [JsonPropertyName("name")]
        public string Nome { set; get; }
        [Display(Name = "Nome Completo")]
        [JsonPropertyName("full_name")]
        public string NomeCompleto { set; get; }
        [JsonPropertyName("language")]
        public string Linguagem { set; get; }
        [Display(Name = "Última Modificação")]
        [JsonPropertyName("updated_at")]
        public DateTime UltimaModificacao { set; get; }
        [Display(Name = "Proprietário")]
        [JsonPropertyName("owner")]
        public UsuarioModel Proprietario { set; get; }
        public List<UsuarioModel> Contribuidores { set; get; }
    }
}
