﻿using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace Model.AuxModel
{
    public class SearchResultAuxModel
    {
        [JsonPropertyName("total_count")]
        public int TotalRepo { set; get; }
        [JsonPropertyName("items")]
        public List<RepositorioModel> Repositorios { set; get; }
    }
}
