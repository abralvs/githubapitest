﻿using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace Model
{
    public class UsuarioModel
    {
        [JsonPropertyName("id")]
        public int Id { set; get; }
        [JsonPropertyName("login")]
        public string Login { set; get; }
        [JsonPropertyName("html_url")]
        [Display(Name = "Url do Perfil")]
        public string UrlPerfil { set; get; }
        [JsonPropertyName("avatar_url")]
        public string UrlFoto { set; get; }
    }
}
