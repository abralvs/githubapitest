﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Persistence
{
    [Table("Favorito")]
    public class Favorito
    {
        [Key]
        [Required]
        public int Id { set; get; }
        [Required]
        public string Nome { set; get; }
        [Required]
        public string NomeCompleto { set; get; }
        [Required]
        public string Proprietario { set; get; }
    }
}
