﻿using System.Data.Entity;

namespace Persistence
{
    public class SQLiteDBContext : DbContext
    {
        public DbSet<Favorito> Favoritos { get; set; }
    }
}
