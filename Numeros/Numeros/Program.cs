﻿using System;
using System.Collections.Generic;

namespace Numeros
{
    class Program
    {

        static void Main(string[] args)
        {
            int[] numeros = new int[5] { 10, 4, 33, 8, 10 };
            double media = calculaMedia(numeros, 0, numeros.Length);
            Console.WriteLine("Média da lista: " + media);
            arrayInverso(numeros, 0, numeros.Length);
            Console.Write("Lista Invertida: ");
            imprime(numeros, 0, numeros.Length);
        }

        static double calculaMedia(int[] numeros, int i, int n)
        {
            if (i == n - 1)
            {
                return numeros[i];
            }

            return i == 0 ? ((numeros[i] + calculaMedia(numeros, i + 1, n)) / n) : (numeros[i] + calculaMedia(numeros, i + 1, n));
     
        }

        static void arrayInverso(int[] numeros, int inicio, int fim)
        {
            if (inicio >= fim)
                return;
            //bubble sort metodo
            int temp = numeros[inicio];
            numeros[inicio] = numeros[fim-1];
            numeros[fim-1] = temp;
            arrayInverso(numeros, inicio + 1, fim - 1);
        }

        static void imprime(int[] numeros, int i, int n)
        {
            if (i >= n)
                return;
            Console.Write(numeros[i] + ".");
            imprime(numeros, i + 1, n);
        }
    }
}
